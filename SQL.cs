﻿using System;
using System.Data.SQLite;
using System.Data;
using System.Windows.Forms;

public class SQL
{
    private SQLiteConnection sqliteConnection;

    public void CreateDB()
    {
        string createDistanz   = "CREATE TABLE `Distanzen` (`Von` INTEGER, `Nach` INTEGER, `Distanz` REAL, `Luftlinie` REAL)";
        string createStationen = "CREATE TABLE `Stationen` (`nr` INTEGER PRIMARY KEY AUTOINCREMENT, `X`	REAL, `Y` REAL, `Typ` TEXT)";
        Query(createDistanz);
        Query(createStationen);
    }

    public bool OpenDB(string dbName)
    {
        sqliteConnection = new SQLiteConnection();
        sqliteConnection.ConnectionString = "Data Source=" + dbName;      //< Alle anderen Parameter werden ignoriert.
        try
        {
            sqliteConnection.Open();
        }
        catch (Exception ex)
        {
            MessageBox.Show("Keine Verbindung zur Datenbank möglich. Fehler Nachricht: " + ex.ToString() + "Bitte Dateipfad überprüfen und Programm neu starten.");
            return false;
        }
        return true;
    }

    public void Query(string sqlString)
    {
        if (sqliteConnection == null)
        {
            MessageBox.Show("Datenbank noch nicht geöffnet.", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
        SQLiteCommand sqlCmd = new SQLiteCommand
        {
            CommandText = sqlString,
            Connection = sqliteConnection
        };
        try
        {
            sqlCmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            MessageBox.Show("Keine Verbindung zur Datenbank. Fehler: " + ex.ToString());
        }
    }

    public DataTable GetData(string query)
    {
        if (sqliteConnection == null)
        {
            MessageBox.Show("Datenbank noch nicht geöffnet.", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return null;
        }

        DataTable data = new DataTable();
        SQLiteDataAdapter myAdapter = new SQLiteDataAdapter();
        myAdapter.SelectCommand = new SQLiteCommand(query, sqliteConnection);
        try
        {
            myAdapter.Fill(data);
        }
        catch (Exception ex)
        {
            MessageBox.Show("Keine Verbindung zur Datenbank möglich. Bitte Internetverbindung prüfen! Fehler: " + ex.ToString());
            data = null;
        }
        return data;
    }

    public void CloseDB()
    {
        // Falls die DB nicht offen war auch ok...
        try
        {
            sqliteConnection.Close();
        }
        catch (Exception)
        {
            // nothing to catch - no message output
            return;
        }
    }
}
